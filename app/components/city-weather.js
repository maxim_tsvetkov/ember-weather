import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
	removeCity: function(city) {
	  this.sendAction('remove', city);
	}
  }
});
