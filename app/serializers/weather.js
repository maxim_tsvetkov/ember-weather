import DS from 'ember-data';

export default DS.JSONSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    payload.coordinatelon = payload.coord.lon;
    payload.coordinatelat = payload.coord.lat;
    payload.country = payload.sys.country;
    payload.sunrise = payload.sys.sunrise;
    payload.sunset = payload.sys.sunset;
    payload.description = payload.weather[0].description;
    payload.temp = payload.main.temp;
    payload.humidity = payload.main.humidity;
    payload.pressure = payload.main.pressure;
    payload.temp_min = payload.main.temp_min;
    payload.temp_max = payload.main.temp_max;
    payload.windspeed = payload.wind.speed;
    payload.winddeg = payload.wind.deg;
    payload.clouds = payload.clouds.all;
    payload.rain = JSON.stringify(payload.rain);
    payload.snow = JSON.stringify(payload.snow);
    payload.date = payload.dt;
    payload.cityname = payload.name;
    payload.cityid = payload.id;    

    delete payload.coord;
    delete payload.sys;
    delete payload.weather;
    delete payload.base;
    delete payload.main;
    delete payload.wind;
    delete payload.dt;
    delete payload.name;

    return this._super(...arguments);
  }
});
