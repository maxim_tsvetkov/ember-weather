import Ember from 'ember';
import config from '../config/environment';

export default Ember.Controller.extend({
  key: config.APP.APIKEY,
  city: '',    
  cityArray: [],  
  currentLocal: 'en',
  localArray: Ember.computed('currentLocal', function(){
    let self = this;
    let arr = ['en', 'de', 'es', 'ru', 'fr', 'zh'];
    let result = [];

    var Local = Ember.Object.extend({
      lang: '',
      isActive: false
    });

    arr.forEach(function(item){
      let url = 'assets/img/' + item + '_16.png';

      let object = Local.create({
        lang: item,
        isActive: false,
        img: url
      });      

      if (object.lang === self.get('currentLocal')){
        object.isActive = true;
      } else {
        object.isActive = false;
      } 

      result.push(object);
    });    

    return result;
  }),  
  actions: {
  	add(name){          
  	  this.store.queryRecord('weather', { q: name, lang: this.get('currentLocal'), units: 'metric', APPID: this.get('key')}).then((city)=>{  	  	
        this.get('cityArray').pushObject(city);
  	  	this.set('city', '');          
	    });  	  
  	},
    remove(city){
      this.get('cityArray').removeObject(city);
    },
    setLocal(local){      
      this.set('currentLocal', local);
    }	
  }
});
