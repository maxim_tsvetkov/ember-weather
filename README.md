# Weather-app. Creater: Max Tsvetkov.  [![Build Status](https://travis-ci.org/MaxFlower/Ember-weather.svg?branch=master)](https://travis-ci.org/MaxFlower/Ember-weather)

Ember application based on [Open Weather Map API](http://openweathermap.org/api)
A short introduction of this app could easily go here.
Feel free to contact me (You can find my contacts on appropriate page after running or in my profile).

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).



