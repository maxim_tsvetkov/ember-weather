/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'weather-app',
    environment: environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    //ENV.rootURL = '/ember-weather-page/'
    ENV.APP.APIHOST = 'http://api.openweathermap.org/data/2.5';
    ENV.APP.APIKEY = 'cb7f79462edd410008232502ce5fa718';
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {      
    ENV.APP.APIHOST = 'http://api.openweathermap.org/data/2.5';
    ENV.APP.APIKEY = 'cb7f79462edd410008232502ce5fa718';
  }

  return ENV;
};
