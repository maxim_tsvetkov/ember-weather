import Ember from 'ember';
import { moduleForModel, test } from 'ember-qunit';
import DS from 'ember-data';

var Weather;
var run = Ember.run;

moduleForModel('weather', 'Unit | Model | weather', {
  beforeEach() {
    Weather = DS.Model.extend({
        coordinatelon: DS.attr('number'),
		coordinatelat: DS.attr('number'),
		country:  DS.attr('string'),
		sunrise:   DS.attr('number'),
		sunset:  DS.attr('number'),
		description:  DS.attr('string'),
		temp:  DS.attr('number'),
		humidity:  DS.attr('number'),
		pressure:  DS.attr('number'),
		temp_min:  DS.attr('number'),
		temp_max:  DS.attr('number'),
		windspeed:  DS.attr('number'),
		winddeg:  DS.attr('number'),
		clouds:  DS.attr('number'),
		rain:  DS.attr('string'),
		snow:  DS.attr('string'),
		date:  DS.attr('number'),
		cityname:  DS.attr('string'),
		cityid: DS.attr('number')
    });
  }
});

test('it model exists', function(assert) {  
  const model = this.subject();
  
  run(function() {
  	model.set('cityname', 'London');
  	assert.equal(model.get('cityname'), 'London');
  });  
});
