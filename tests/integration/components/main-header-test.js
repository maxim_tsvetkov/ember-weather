import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('main-header', 'Integration | Component | main header', {
  integration: true
});

test('it renders', function(assert) {  
  assert.expect(0);
  this.render(hbs`{{main-header}}`);
});
